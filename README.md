# Package samtools Version 1.9
https://github.com/samtools/samtools

SAM Tools provide various utilities for manipulating alignments in the SAM format, including sorting, merging, indexing and generating alignments in a per-position format

Package installation using Miniconda3 V4.7.12 All packages are in /opt/miniconda/bin & are in PATH samtools Version: 1.9
Singularity container based on the recipe: Singularity.samtools_v1.9
## Local build:
```bash
sudo singularity build samtools_v1.9.sif Singularity.samtools_v1.9
```
## Get image help:

```bash
singularity run-help samtools_v1.9.sif
```
### Default runscript: samtools
## Usage:
```bash
./samtools_v1.9.sif --help
```
or:
```bash
singularity exec samtools_v1.9.sif samtools --help
```
image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml
can be pull (singularity version >=3.3) with:


```bash
singularity pull samtools_v1.9.sif oras://registry.forgemia.inra.fr/singularity/bioinfo/samtools_v.1.9/samtools_v.1.9:latest
```
